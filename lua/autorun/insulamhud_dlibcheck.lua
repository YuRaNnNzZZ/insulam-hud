local function checkForDLib()
	if DLib then return end -- we're 100% good

	if chat and chat.AddText then
		chat.AddText(Color(255, 63, 63), "WARNING: ", Color(255, 255, 255), "Estranged Act II HUD requires DLib installed to work.")
		chat.AddText(Color(255, 255, 255), "You can get it here: ", Color(31, 127, 255), "https://steamcommunity.com/workshop/filedetails/?id=1153306104")
	elseif not game.SinglePlayer() then
		print("############################ WARNING!!! ############################")
		print("#          Estranged: Act II HUD requires DLib installed.          #")
		print("#  https://steamcommunity.com/workshop/filedetails/?id=1153306104  #")
		print("####################################################################")
	end
end

hook.Add("InitPostEntity", "InsulamHUD_DLibCheck", checkForDLib)