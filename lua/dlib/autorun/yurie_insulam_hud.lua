INSULAMHUD = INSULAMHUD or {}

local basepath = "dlib/insulamhud"

hook.DisableHook("InitPostEntity", "InsulamHUD_DLibCheck")

if CLIENT then
	include(basepath .. "/init.lua")

	return
end

for _, filename in pairs(file.FindRecursive(basepath, "/*.lua")) do
	AddCSLuaFile(filename)
end