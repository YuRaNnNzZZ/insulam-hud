INSULAMHUD = INSULAMHUD or {}

local INSULAMHUD = INSULAMHUD

local HUDCommons = DLib.HUDCommons

local Mat_Health_BG = Material("vgui/yurie/insulamhud/healthindicatorbackground.png", "smooth")
local Mat_Health_FG = Material("vgui/yurie/insulamhud/custom/healthindicatorforeground.png", "smooth")

local Mat_Armor_FG = Material("vgui/yurie/insulamhud/custom/healthindicatorglow.png", "smooth")

local Pos_Health = INSULAMHUD:DefinePosition("health", 0.075, 0.9)

local Scale_Health = INSULAMHUD:CreateConVar("scale_health", 0.1, "Health indicator scale (fraction of screen height)", true)

local Col_White = Color(255, 255, 255, 255)
local Col_Health = INSULAMHUD:CreateColorN("color_health_fill", "Health Indicator Gradient", 255, 0, 0)
local Col_Armor = INSULAMHUD:CreateColorN("color_armor", "Armor Glow", 0, 255, 255)

INSULAMHUD.HealthEnabled = INSULAMHUD.HealthEnabled or INSULAMHUD:CreateConVar("health_enabled", "1", "Enable health indicator")
INSULAMHUD.ArmorEnabled = INSULAMHUD.ArmorEnabled or INSULAMHUD:CreateConVar("armor_enabled", "1", "Enable armor glow over health indicator")

function INSULAMHUD:DrawHealth(ply)
	if not self.HealthEnabled:GetBool() or not self:GetVarAlive() and not HUDCommons.IsInEditMode() then return end

	local size = ScrH() * Scale_Health:GetFloat():max(0.0001)

	local x, y = Pos_Health()

	surface.SetDrawColor(Col_White)
	surface.SetMaterial(Mat_Health_BG)
	surface.DrawTexturedRect(x - size * .5, y - size * .5, size, size)

	local ArmorRatio = (self:GetVarArmor() / self:GetVarMaxArmor()):clamp(0, 1)
	local HealthRatio = (self:GetVarHealth() / self:GetVarMaxHealth()):clamp(0, 1)

	if HUDCommons.IsInEditMode() then
		local val = CurTime():cos() * .5 + .5
		ArmorRatio = val
		HealthRatio = val
	end

	if ArmorRatio > 0 and self.ArmorEnabled:GetBool() then
		surface.SetDrawColor(Col_Armor())
		surface.SetMaterial(Mat_Armor_FG)

		render.SetScissorRect(x - size * .5, y - size * .5 + size * (1 - ArmorRatio), x + size * .5, y + size * .5, true)
		surface.DrawTexturedRect(x - size * .5, y - size * .5, size, size)
		render.SetScissorRect(0, 0, 0, 0, false)
	end

	if HealthRatio > 0 then
		surface.SetDrawColor(Col_Health())
		surface.SetMaterial(Mat_Health_FG)

		render.SetScissorRect(x - size * .5, y - size * .5 + size * (1 - HealthRatio), x + size * .5, y + size * .5, true)
		surface.DrawTexturedRect(x - size * .5, y - size * .5, size, size)
		render.SetScissorRect(0, 0, 0, 0, false)
	end
end

INSULAMHUD:AddPaintHook("DrawHealth")

local BlockElement = {
	["CHudHealth"] = true,
	["CHudBattery"] = true,
}

function INSULAMHUD:HealthShouldDraw(element)
	if BlockElement[element] and self.HealthEnabled:GetBool() then
		return false
	end
end

INSULAMHUD:AddHookCustom("HUDShouldDraw", "HealthShouldDraw")
