INSULAMHUD = INSULAMHUD or {}

local INSULAMHUD = INSULAMHUD

local HUDCommons = DLib.HUDCommons
local ScreenSize = ScreenSize

local Col_Ammo = INSULAMHUD:CreateColorN("color_ammo", "Ammo Counter", 255, 255, 255)
local Col_Shadow = Color(0, 0, 0)

local Pos_Ammo_Primary = INSULAMHUD:DefinePosition("ammo_primary", 0.9, 0.1)
local Pos_Ammo_Secondary = INSULAMHUD:DefinePosition("ammo_secondary", 0.9, 0.25)

INSULAMHUD.AmmoEnabled = INSULAMHUD.AmmoEnabled or INSULAMHUD:CreateConVar("ammo_enabled", "1", "Enable custom ammo counter")

local Font_Clip = INSULAMHUD:CreateScalableFont("AmmoClip", {
	font = "Oswald",
	size = 78,
})

local Font_Reserve = INSULAMHUD:CreateScalableFont("AmmoReserve", {
	font = "Oswald",
	size = 40,
})

function INSULAMHUD:DrawAmmo(ply)
	if not self.AmmoEnabled:GetBool() or not self:GetVarAlive() or not self:HasWeapon() and not HUDCommons.IsInEditMode() then return end

	local DrawPrimary, DrawSecondary = self:ShouldDisplayAmmo(), self:ShouldDisplaySecondaryAmmo()

	local clip1, ammo1 = self:GetDisplayClip1(), self:GetDisplayAmmo1()
	local clip2, ammo2 = self:GetDisplayClip2(), self:GetDisplayAmmo2()

	if HUDCommons.IsInEditMode() then
		DrawPrimary, clip1, ammo1 = true, 255, 9999
		DrawSecondary, clip2, ammo2 = true, 255, 9999
	end

	if DrawPrimary then
		local x, y = Pos_Ammo_Primary()

		if not self:ShouldDisplayAmmoReady() and not HUDCommons.IsInEditMode() then
			clip1 = ammo1
			ammo1 = -1
		end

		local clipw, cliph
		do
			surface.SetFont(Font_Clip.REGULAR)
			clipw, cliph = surface.GetTextSize("" .. clip1)
			cliph = cliph * .75
		end

		local ammow, ammoh
		do
			surface.SetFont(Font_Reserve.REGULAR)
			ammow, ammoh = surface.GetTextSize("" .. ammo1)
		end

		local ColorText = Col_Ammo()
		local ColorShadow = Col_Shadow

		if clip1 >= 0 then
			draw.SimpleText(clip1, Font_Clip.REGULAR, x + 1, y + 1, ColorShadow)
			draw.SimpleText(clip1, Font_Clip.REGULAR, x, y, ColorText)
		end

		if ammo1 >= 0 then
			local MagPosX = (x + clipw) - ammow

			draw.SimpleText(ammo1, Font_Reserve.REGULAR, MagPosX + 1, y + cliph + 1, ColorShadow)
			draw.SimpleText(ammo1, Font_Reserve.REGULAR, MagPosX, y + cliph, ColorText)
		end
	end

	if DrawSecondary then
		local x, y = Pos_Ammo_Secondary()

		if not self:ShouldDisplaySecondaryAmmoReady() and not HUDCommons.IsInEditMode() then
			clip2 = ammo2
			ammo2 = -1
		end

		local clipw, cliph
		do
			surface.SetFont(Font_Clip.REGULAR)
			clipw, cliph = surface.GetTextSize("" .. clip2)
			cliph = cliph * .75
		end

		local ammow, ammoh
		do
			surface.SetFont(Font_Reserve.REGULAR)
			ammow, ammoh = surface.GetTextSize("" .. ammo2)
		end

		local ColorText = Col_Ammo()
		local ColorShadow = Col_Shadow

		if clip2 >= 0 then
			draw.SimpleText(clip2, Font_Clip.REGULAR, x + 1, y + 1, ColorShadow)
			draw.SimpleText(clip2, Font_Clip.REGULAR, x, y, ColorText)
		end

		if ammo2 >= 0 then
			local MagPosX = (x + clipw) - ammow

			draw.SimpleText(ammo2, Font_Reserve.REGULAR, MagPosX + 1, y + cliph + 1, ColorShadow)
			draw.SimpleText(ammo2, Font_Reserve.REGULAR, MagPosX, y + cliph, ColorText)
		end
	end
end

INSULAMHUD:AddPaintHook("DrawAmmo")

local BlockElement = {
	["CHudAmmo"] = true,
	["CHudSecondaryAmmo"] = true,
}

function INSULAMHUD:AmmoShouldDraw(element)
	if BlockElement[element] and self.AmmoEnabled:GetBool() then
		return false
	end
end

INSULAMHUD:AddHookCustom("HUDShouldDraw", "AmmoShouldDraw")
