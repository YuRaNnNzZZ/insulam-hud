INSULAMHUD = INSULAMHUD or {}

local INSULAMHUD = INSULAMHUD

local HUDCommons = DLib.HUDCommons

local Mat_Flashlight_BG = Material("vgui/yurie/insulamhud/flashlightindicatorbackground.png", "smooth")
local Mat_Flashlight_FG = Material("vgui/yurie/insulamhud/custom/flashlightindicatorforeground.png", "smooth")

local Mat_Oxygen_BG = Material("vgui/yurie/insulamhud/oxygenindicatorbackground.png", "smooth")
local Mat_Oxygen_FG = Material("vgui/yurie/insulamhud/custom/oxygenindicatorforeground.png", "smooth")

local Pos_Battery = INSULAMHUD:DefinePosition("battery", 0.15, 0.9)

local Col_White = Color(255, 255, 255, 255)

local Col_Flashlight_FG_On = INSULAMHUD:CreateColorN("color_flashlight_fg_on", "Flashlight (On)", 255, 162, 0)
local Col_Flashlight_FG_Off = INSULAMHUD:CreateColorN("color_flashlight_fg_off", "Flashlight (Off)", 192, 122, 0)

local Col_Oxygen_FG = INSULAMHUD:CreateColorN("color_oxygen_fg", "Oxygen", 0, 100, 255)

local Scale_Battery = INSULAMHUD:CreateConVar("scale_battery", 0.1, "Flashlight battery indicator scale (fraction of screen height)", true)

INSULAMHUD.BatteryEnabled = INSULAMHUD.BatteryEnabled or INSULAMHUD:CreateConVar("battery_enabled", "1", "Enable suit energy display")

local CVar_Suit = GetConVar("gmod_suit")

function INSULAMHUD:DrawBattery(ply)
	if not CVar_Suit:GetBool() or not self.BatteryEnabled:GetBool() or not self:GetVarAlive() and not HUDCommons.IsInEditMode() then return end

	local size = ScrH() * Scale_Battery:GetFloat():max(0.0001)

	local x, y = Pos_Battery()

	local IsOxygenMode = ply:WaterLevel() > 2 or ply:IsSprinting()

	local BatteryRatio = ply:GetSuitPower() / 100

	if HUDCommons.IsInEditMode() then
		BatteryRatio = CurTime():cos() * .5 + .5
		IsOxygenMode = CurTime() % 2 > 1
	end

	surface.SetDrawColor(Col_White)
	surface.SetMaterial(IsOxygenMode and Mat_Oxygen_BG or Mat_Flashlight_BG)
	surface.DrawTexturedRect(x - size * .5, y - size * .5, size, size)

	if BatteryRatio > 0 then
		if IsOxygenMode then
			surface.SetDrawColor(Col_Oxygen_FG())
			surface.SetMaterial(Mat_Oxygen_FG)
		else
			surface.SetDrawColor(ply:FlashlightIsOn() and Col_Flashlight_FG_On() or Col_Flashlight_FG_Off())
			surface.SetMaterial(Mat_Flashlight_FG)
		end

		render.SetScissorRect(x - size * .5, y - size * .5 + size * (1 - BatteryRatio), x + size * .5, y + size * .5, true)
		surface.DrawTexturedRect(x - size * .5, y - size * .5, size, size)
		render.SetScissorRect(0, 0, 0, 0, false)
	end
end

INSULAMHUD:AddPaintHook("DrawBattery")

function INSULAMHUD:BatteryShouldDraw(element)
	if element == "CHudSuitPower" and self.BatteryEnabled:GetBool() then
		return false
	end
end

INSULAMHUD:AddHookCustom("HUDShouldDraw", "BatteryShouldDraw")
