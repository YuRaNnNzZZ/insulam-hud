_G.INSULAMHUD = DLib.HUDCommons.BaseMeta("insulam_hud", "Estranged: Act II HUD")

local INSULAMHUD = INSULAMHUD

include("xhair.lua")
include("health.lua")
include("battery.lua")
include("ammo.lua")

function INSULAMHUD:PopulateDefaultSettingsOe(panel)
	panel:Help("")

	panel:Help("Estranged: Act II HUD - a fan-made recreation.")
	panel:Help("Textures, reference implementation and design by Alan Edwardes - https://playestranged.com")
	panel:Help("Main font: Oswald by The Oswald Project Authors - https://github.com/googlefonts/OswaldFont")
	panel:Help("Lua code by DBotThePony (base) and YuRaNnNzZZ (rest)")
end

concommand.Add(INSULAMHUD:GetID() .. "_font_license", include("fontlicense.lua"))
