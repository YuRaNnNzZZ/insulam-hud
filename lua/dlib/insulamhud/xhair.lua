INSULAMHUD = INSULAMHUD or {}

local INSULAMHUD = INSULAMHUD

local Mat_Crosshair_Dot = Material("vgui/yurie/insulamhud/dot.png", "mips smooth")
local Mat_Crosshair_Gun = Material("vgui/yurie/insulamhud/crosshair.png", "mips smooth")

local Scale_Crosshair_Dot = INSULAMHUD:CreateConVar("scale_crosshair_dot", 0.0125, "Crosshair dot scale (fraction of screen height)", true)
local Scale_Crosshair_Gun = INSULAMHUD:CreateConVar("scale_crosshair_cross", 0.025, "Crosshair cross scale (fraction of screen height)", true)

local Col_Crosshair = INSULAMHUD:CreateColorN("color_crosshair", "Crosshair", 255, 255, 255)

INSULAMHUD.CrosshairEnabled = INSULAMHUD.CrosshairEnabled or INSULAMHUD:CreateConVar("crosshair_enabled", "1", "Enable custom crosshair")
INSULAMHUD.CrosshairDynamic = INSULAMHUD.CrosshairDynamic or INSULAMHUD:CreateConVar("crosshair_dynamic", "1", "Сrosshair follows aim direction\n(Also applies to SWEPs crosshairs)")

local HookCheck = false

function INSULAMHUD:DrawReticule(ply)
	if not self.CrosshairEnabled:GetBool() or not self:GetVarAlive() then return end

	local ShouldDraw = true

	HookCheck = true
	ShouldDraw = hook.Run("HUDShouldDraw", "CHudCrosshair") ~= false
	HookCheck = false

	if not ShouldDraw then return end

	local wep = self:GetWeapon()

	if IsValid(wep) and wep.DrawCrosshair == false or wep.HUDShouldDraw and wep:HUDShouldDraw("CHudCrosshair") == false then
		ShouldDraw = false
	end

	local x, y = ScrW() * .5, ScrH() * .5

	if self.CrosshairDynamic:GetBool() and IsValid(ply) then
		local _TraceResult = ply:GetEyeTrace()

		if _TraceResult.HitPos then
			local _ToScreenData = _TraceResult.HitPos:ToScreen()

			x, y = _ToScreenData.x, _ToScreenData.y
		end
	end

	if ShouldDraw and IsValid(wep) and wep.DoDrawCrosshair then
		local status = wep:DoDrawCrosshair(x, y)
		ShouldDraw = status ~= true
	end

	if ShouldDraw then
		local IsFirearm = (self:ShouldDisplayAmmo() or self:ShouldDisplaySecondaryAmmo()) and not (self:GetVarInVehicle() and not self:GetVarWeaponsInVehicle())

		local scale, mat = IsFirearm and Scale_Crosshair_Gun:GetFloat() or Scale_Crosshair_Dot:GetFloat(), IsFirearm and Mat_Crosshair_Gun or Mat_Crosshair_Dot

		local size = ScrH() * scale:max(0.0001)

		surface.SetMaterial(mat)
		surface.SetDrawColor(Col_Crosshair())
		surface.DrawTexturedRect(x - size * .5, y - size * .5, size, size)
	end
end

INSULAMHUD:AddPaintHook("DrawReticule")

function INSULAMHUD:CrosshairShouldDraw(element)
	if element == "CHudCrosshair" and not HookCheck and self.CrosshairEnabled:GetBool() then
		return false
	end
end

INSULAMHUD:AddHookCustom("HUDShouldDraw", "CrosshairShouldDraw")