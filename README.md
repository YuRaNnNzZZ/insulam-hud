# Estranged: Act II HUD (aka Insulam HUD)

This is a simple recreation of the HUD from [Estranged: Act II](https://store.steampowered.com/app/582890/Estranged_Act_II/) for Garry's Mod.
Built using HUDCommons from [DLib](https://gitlab.com/DBotThePony/DLib) (required to work)

## How to use
1. Install [DLib](https://gitlab.com/DBotThePony/DLib) (Build it yourself or download a pre-built copy [here](https://gitlab.com/DBotThePony/DLib/-/jobs))
1. (Optional) Install [Limited Oxygen and Flashlight](https://gitlab.com/DBotThePony/DBotProjects/tree/develop/limitedflandoxygen) addon to use flashlight battery indicator
1. Clone or download this repository into your addons folder
1. Launch the game and configure the HUD for your tastes

## Legal stuff
- The code (Lua files) is licensed under [MIT License](https://opensource.org/licenses/MIT)
- The textures (used with permission) and [reference implementation](https://github.com/alanedwardes/Estranged.Core/blob/master/Source/EstCore/Private/EstPlayerHUD.cpp) is by Alan Edwardes - https://iamestranged.com
- The main font is Oswald by [The Oswald Project Authors](https://github.com/googlefonts/OswaldFont), licensed under [SIL Open Font License 1.1](https://github.com/googlefonts/OswaldFont/blob/master/OFL.txt)
